\chapter{Einführung}\label{cha:intro}

\section{Motivation}

Fortgeschrittene Programmanalysen erlauben leichtere Detektion von Fehler in dem Programmcode, ermöglichen Programmoptimierung und Programmverifikation. Viele moderne Compiler, Debugger, IDEs und Testframeworks hängen stark von diesen Analysen ab. Es existieren zahlreiche statische und dynamische Varianten der Programmanalyse. Statische Analysen haben den Vorteil, dass sie bei der Kompilierung ausgeführt werden können, ohne Notwendigkeit das Programm in der Produktionsumgebung ausführen zu müssen. Die Ergebnisse der Analyse sind  abstrakt und nicht plattform- oder datenabhängig. Dynamische Analysen haben dagegen den Vorteil, dass sie mit konkreten Daten und Programmzuständen arbeiten, und somit sehr präzise und konkrete Aussagen über die Korrektheit der Programme treffen können. 

Zwei große Bereiche der statischen Programmanalyse sind die Kontroll- und Datenflussanalysen. Das Ziel der Kontrollflussanalyse ist den Kontrollfluss eines Programms zu bestimmen. Kontrollfluss ist eine Abfolge von Instruktionen, die nach einander ausgeführt werden. Da in einem Kontrollfluss Verzweigungen und Schleifen vorkommen können, wird dieser als ein gerichteter Graph von Instruktionen dargestellt. Dieser Graph wird Kontrollflussgraph genannt. Das Ziel der Datenflussanalyse ist es, herauszufinden welche Werte entlang des Kontrollflussgraphen von einem Programm auftreten und wie der Kontrollfluss diese Werte beeinflusst. 

Es exisitieren zahlreiche Ansätze für die Bestimmung der Datenflussinformationen. Diese Analysen können unterschiedliche Datenabhängigkeiten mit unterschiedlicher Präzision aufdecken. Einer der Ansätze ist die Pfadprädikat-Analyse \cite{heinzesparse}. Ein Pfadprädikat ist ein logischer Ausdruck, welcher beschreibt, auf welchen Weg der Wert der Variable entstanden ist. Diese Informationen eignen sich für ein breites Spektrum von Analysen. Da es logische Ausdrücke sind, kann man mit einem SMT-solver diese Ausdrücke ableiten und dadurch den Programmcode optimieren. So können beispielsweise alle Konstanten gefaltet werden oder nicht erreichbarer Code eliminiert werden. 

Das Nutzen von den Prädikaten wird an dem folgenden Beispiel demonstriert. Gegeben sei ein Programm, das in dem Quelltext \ref{lst:example} zu sehen ist. 

\begin{center}
\begin{minipage}{0.42\linewidth}% oder andere Breite
\begin{lstlisting}[caption={Einführungsbeispiel}, captionpos=b, label={lst:example}]
int c = 5;
int y = 0;
int x = 2 * c;
if(x < 10){
	y = y * 10;
}
int z = y + 10;
\end{lstlisting}
\end{minipage}
\end{center}

Der Ausdruck \texttt{int c = 5;} wird mit dem Prädikat $c = 5$ beschrieben. Genauso wird der Ausdruck \texttt{int y = 0;} abgeleitet, sein zugehöriger Prädikat ist $y = 0$. Bei dem Ausdruck \texttt{int x = 2 * c;} entsteht ein komplexerer Prädikat, nämlich $(x = 2 * c) \land (c = 5)$. Da beide Teilausdrucke von diesem Prädikat gelten, können mittels Transitivität ableiten werden, so dass $x = 2 * 5$ und folglich $x = 10$. Die nächste Instruktion ist eine if-Anweisung mit der Bedienung \texttt{(x < 10)}. Diese Bedienung kann sofort ausgewertet werden, weil der Wert von $x$ konstant ist. Da $10 < 10$ immer als \textit{false} ausgewertet wird, wird die Bedienung niemals ausgeführt, weshalb der Code von dieser if-Anweisung komplett eliminiert werden kann. Es bleibt nun der letzte Ausdruck \texttt{int z = 2 * y;}. Dieser kann, ähnlich wie die Variable $x$, mit dem Prädikat $(z = y + 10) \land (y = 0)$ beschrieben werden. Daraus wird der Wert der Variable $z$ abgeleitet. $((z = y + 10) \land (y = 0)) \models z = 0 + 10 \rightarrow z = 10$. Nachdem alle Prädikate abgeleitet sind, kann man die optimierte Version von diesem Programm rekonstruieren, in dem man die Variablendefinitionen durch die Definitionen ersetzt, die bei der Ableitung entstanden sind. Die rekonstruierte Version ist in dem Quelltext \ref{lst:exampleOpt} zu sehen.

\begin{lstlisting}[caption={Programm, nach der Optimierung durch Ableitung von Variablenprädikaten}, captionpos=b, label={lst:exampleOpt}]
int c = 5;
int y = 0;
int x = 10;
int z = 10;
\end{lstlisting}
Wie genau die Prädikate erzeugt werden, wird im Kapitel \ref{cha:analysis} behandelt. 

\section{Ziele der Arbeit}
Das Ziel der Arbeit ist ein System zu entwickelt, das die Prädikate nach dem in dem Paper \cite{heinzesparse} beschriebenen Verfahren ableitet. Es werden zwei Varianten der Analyse implementiert, einmal ohne Benutzung von Pfaddesignatoren und einmal mit der Benutzung von Pfaddesignatoren. Die beiden Analysen werden mit Hilfe von dem SOOT-Framework implementiert. Es muss gezeigt werden, wie stark Performance durch die Benutzung von Pfaddesignatoren beeinflusst wird, und wie stark die Genauigkeit von der Prädikatenmenge in der zweiten Analyse verbessert wird.

\section{Struktur der Arbeit}
In dem nächsten Kapitel (\ref{cha:basics}) werden alle notwendigen Grundlagen und -begriffe der Arbeit eingeführt. Zum einem wird die SSA-Form erleutert und ihr Nutzen in der Analyse. Zum aneren werden die Variablenprädikate aus \cite{heinzesparse} eingeführt. Am Ende des Kapitels wird ein kurzer Überblick über den SOOT-Framework \cite{sootframework} angegeben.

In Kapitel \ref{cha:analysis} wird die zu implementierende Analyse genauer betrachtet. Zuerst wird der zugrundeliegende Algorithmus betrachtet und alle notwendigen Transformationen von dem Kontrollflussgraph. In dem zweiten Abschnitt wird gezeigt, wie mittels so genannter Pfaddesignatoren die Genauigkeit der Analyse verbessert werden kann. \par 
In Kapitel \ref{cha:impl} wird die Implementierung des Systems genauer betrachtet. Zuerst wird gezeigt, wie die SOOT API benutzt wird, um die Analyse ausführen zu können und welche Instrumente SOOT zur Verfügung stellt. Dann wird die Architektur von dem ganzen System präsentiert. Danach wird es genauer auf die Implementierung von dem beiden Analysen eingegangen. \par 
In Kapitel \ref{cha:eval} werden die Ergebnisse der einfachen und verbesserten Analysen verglichen. Zuerst wird das Benchmarking Verfahren mittels \textit{DaCapo}-Benchmark \cite{dacapobenchmark} vorgestellt. Im Anschluss werden die Ergebnisse interpretiert.\par 
Zum Schluss werden in Kapitel \ref{cha:summary} die Ergebnisse der Arbeit zusammengefasst.

