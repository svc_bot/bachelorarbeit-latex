\chapter{Analyse}\label{cha:analysis}
In diesem Kapitel wird in dem ersten Abschnitt die Funktionsweise der Analyse näher betrachtet. In dem zweiten Abschnitt wird gezeigt, wieso die Analyse ungenau wird, und wie dieses Problem mit Hilfe von Pfaddesignatoren verbessert werden kann. 

\section{Funktionsweise der Analyse}
In der klassischen Datenflussanalyse wird der gesamte Programmzustand über den vollständigen Kontrollfluss weitergeleitet. Dabei wird iterativer Round-Robin Algorithmus benutzt, der iterativ alle Knoten in dem Kontrollflussgraph in einer fixierten Ordnung besucht und dabei den Zustand von den Knoten neu berechnet, bis ein Fixpunkt erreicht wird \cite{cooper2002iterative}. Um die Variablenprädikate erzeugen zu können, muss man die Werte einer Variable auf unterschiedliche Kontrollflusspfaden unterscheiden können. Durch die Verwendung der SSA-Form wird dieses Problem automatische gelöst, weil die Variablen nur eine statische Definition haben. Statische Definition hat zwei wichtige Folgen: die Variablen können als Werte interpretiert werden, was ermöglicht die Ableitung von atomaren Prädikaten. Zweite Folge ist, dass das einzige Programmteil, das für den Wert der Variable wichtig ist, ist vollständig durch die Def-Use-Ketten erreichbar. Dieser Ansatz der Datenflussanalyse, wo der Datenfluss nur entlang der Def-Use-Ketten propagiert wird, wird Sparse Analysis genannt. 

Da es in dieser Analyse nicht notwendig ist die Programmzustände durch den ganzen Kontrollfluss zu propagieren, kann ein effizienterer iterativer Algorithmus benutzt werden, nämlich der \textit{iterative worklist Algorithmus} \ref{fig:sparseAlgorithm}. Die Idee von diesem Algorithmus ist einfach: Alle Knoten des Kontrollflussgraphen werden in eine Arbeitsliste hinzugefügt. Es wird ein Element aus der Liste entnommen, und für dieses wird neues Prädikat berechnet. Falls das neu-berechnete Prädikat von dem alten Prädikat sich unterscheidet, wird das alte Prädikat durch das neue ersetzt. Dabei müssen alle Knoten, die den aktuell bearbeitenden Knoten benutzen, wieder in die Arbeitsliste hinzugefügt werde, außer sie sind schon in der Liste vorhanden. Wenn keine neue Prädikate berechnet werden, d.h. wenn ein Fixpunkt erreicht wird, wird die Liste leer, und der Algorithmus terminiert.

Damit die Information über Verzweigungs- und Schleifeninvarianten in den Datenfluss integriert wird, wird der Kontrollflussgraph mit Assertions erweitert. Assertions werden in den then- und else-Zweig einer if-Anweisung bzw. in den Schleifenkörper platziert. Durch Assertions werden neue Variablen definiert, für die garantiert wird, dass die Verzweigungs- bzw. Schleifenbedienung für den Zweig erfüllt wird. Alle Benutzungen von den Variablen aus der Verzweigungs- bzw. Schleifenbedienung müssen durch die neu-definierten Variablen ersetzt werden. In der Abbildung \ref{fig:generalassertion} wird die Umwandlung illustriert. 

\begin{figure}[!ht]
\label{fig:bspassertions}
	\centering
	\begin{subfigure}[b]{0.49\textwidth}
		\begin{lstlisting}
		int x,y,z;
		...
		if (x < y){
			z_1 = x - y;		
		} else {
			z_2 = x + y;
		}
		\end{lstlisting}
        \caption{Ein Beispielprogramm in der SSA Form}
        \label{fig:normalSSA}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
 		\begin{lstlisting}
		int x,y,z;
		...
		if (x < y){
			(x_1,y_1) = assert(x < y)
			z_1 = x_1 - y_2;		
		} else {
			(x_2,y_2) = assert(x => y)
			z_2 = x_2 + y_2;
		}
		\end{lstlisting}
        \caption{mit SSA}
        \label{fig:generalassertion}
    \end{subfigure}
    \caption{Beispiel für die Umformung zu SSA-Form und anschließende Erweiterung mit Assertions}
\end{figure}

Der Algorithmus für Erzeugung der Variablenprädikate ist in der rechten Hälfte der Abbildung \ref{fig:sparseAlgorithm} zu sehen. Die Berechnung von Prädikaten unterscheidet sich in der Abhängigkeit von dem Instruktionstyp. Bei einer Konstantenzuweisung $x = c$ ist das dazugehörige Prädikat $\{\{x = c\}\}$. In allen anderen Fällen muss das resultierende Prädikat mit einem \glqq Killset\grqq $\overline{Pred_x}$ geschnitten werden. Die Menge $\overline{Pred_x}$ enthält alle atomare Prädikate, in denen die Variable x nicht vorkommt. Der Schnitt mit dieser Menge resultiert darin, dass alle atomare Prädikate, wo x vorkommt, werden aus dem Prädikat von x eliminiert. Dieser Schritt ist notwendig, um die Inkonsistenzen in den Prädikatenmengen zu vermeiden. Solche Inkonsistenzen können in Schleifen entstehen, \zB wenn eine Variable iterativ inkrementiert wird: \texttt{while(x op const){ y = assert(x op cosnt); y = y + 1; }}. Ohne Killset wird das Prädikat von x die Form haben: \texttt{\{\{y = y + 1, y = y + 2, \dots y = y + n, x op const, y = x\}\}}. Es werden viele Berechnungen gemacht und es entstehen viele Prädikate, die das Gleiche ausdrücken wie das gleiche Prädikat geschnitten mit $\overline{Pred_x}$: \texttt{\{\{y = y + 1, x op const, y = x\}\}}. Da die Killsets während der Ausführung von dem Algorithmus sich nicht ändern, können diese einmal vor dem Beginn der Ausführung von dem worklist-Algorithmus berechnet werden. 

Noch eine Besonderheit gibt es bei der Berechnung von dem Prädikat einer komplexen Zuweisung. Hier muss eine Vereinigung $p \cup q | p \in pred(def(y), q \in pred(def(z)))$ von zwei Prädikaten gebildet werden. Dabei müssen alle möglichen Paare von Pfadprädikaten aus p und q gebildet werden. Die Pfadprädikate in jedem solchen Paar werden vereinigt, indem alle atomaren Prädikate aus diesen zwei Mengen vereinigt werden.  

Die letzte Besonderheit ist bei der Berechnung von dem Prädikat der $\Phi$-Funktion. Die Menge $\bigcap_{v \in V} \overline{Pred_x}$ ist eine Schnittmenge von Killsets von allen $\Phi$-Funktionen aus einem Basisblock. Dies war einer der Hauptgrunde den Blockgraph statt die Chain als Datenstruktur zu verwenden. 

\begin{figure}[hbtp]
\centering
\includegraphics[scale=0.66]{./img/sparseAlgorithm.PNG}
\caption{Algorithmus aus dem Referenz-Paper \cite{heinzesparse}}
\label{fig:sparseAlgorithm}
\end{figure}

\section{Erweiterte Analyse}
Die einfache Variante von der Analyse produziert viele falsch-positive Ergebnisse, Prädikate, die bei der Laufzeit unter keinen Umständen entstehen können. Beispiel \ref{fig:falsepositives}:

\begin{figure}[hbtp]
\centering
\includegraphics[scale=0.8]{./img/spuriusDataFlow.PNG}
\caption{Beispiel für falsche Prädikate aus \cite{heinzesparse}}
\label{fig:falsepositives}
\end{figure}

Der Fehler befindet sich in dem Pfadprädikat von der Variable $c_1$. Die Prädikate $\{a_1 = 2, a_3 = a_1, b_2 = 2, b_3 = b_2, c_1 = a_3 + b_3\}$ und $\{a_2 = 3, a_3 = a_2, b_1 = 3, b_3 = b_1, c_1 = a_3 + b_3 \}$ können bei der Laufzeit nicht entstehen, weil das heißen würde, dass sowohl \textit{then}- als auch \textit{else}-Zweig ausgeführt worden sind, was in einer korrekt funktionierenden Laufzeitumgebung nicht möglich sein kann. 
Um Prädikate von sich ausschließenden Kontrollflusspfade zu entfernen, werden Pfaddesignatoren eingeführt. Ein Pfaddesignator $\delta \in (N \times \mathbb{N})$ ist eine Relation von Knoten aus den Kontrollflussgraph und Pfadindizes, so dass gilt $\forall n \in N: (n,i) \in \delta \land (n,j) \in \delta \rightarrow i = j$ \cite[10]{heinzesparse}. Mit anderen Worten, ein Pfaddesignator kann keine sich ausschließende Pfade enthalten. Um Prädikate mit Pfaddesignatoren erzeugen zu können, muss Override-Funktion von Pfaddesignatoren definiert werden: 
\[ \delta \oplus \gamma \{ (x,i)| (x,i) \in \delta \land \nexists (x,j)\in \gamma \} \cup \{(y,i)| (y,i)\in \gamma\}\].

Die Berechnung von dem Prädikat von einer Konstante bleibt die selbe wie in dem Algorithmus \ref{fig:sparseAlgorithm} mit dem Unterschied, dass es noch es leerer Pfaddesignator dazu kommt. Die einfache Zuweisung ist auch nicht viel anderes. Der Pfaddesignator wird von der zugewiesenen Variable übernommen. Genau so bei einer Assertion. Bei der $\Phi$-Funktion muss der Pfaddesignator von $x_j$ mit $(node(x), j)$ überschrieben werden. Das wichtigste passiert in der komplexen Zuweisung. Es werden alle mögliche Paare aus $(\delta, p)\in pred*(y)$ und $(\gamma, q)\in pred*(z)$ gebildet. Die Kombinationen, wo $\delta$ und $\gamma$ keine Definite Relation haben, d.h. ihre Pfaddesignatoren enthalten sich ausschließende Wege, werden ignoriert.



