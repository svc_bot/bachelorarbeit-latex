% =============================================================================
% dbis-thesis.cls
% Dokumentklasse fuer Abschlussarbeiten am DBIS-Lehrstuhl der FSU Jena,
% basiert auf diplarb.sty von Christoph Gollmick (05.09.2002) 
%
% Autor:             Matthias Liebisch (2011-05-16)
% Letzte "Anderung:  Matthias Liebisch (2012-01-25)
% =============================================================================
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{thesis}[2016/06/30 v1.0 thesis]
\typeout{Dokumentklasse fuer Abschlussarbeiten am DBIS-Lehrstuhl der FSU Jena}
\LoadClass[12pt,a4paper,twoside,BCOR=10mm]{book}
\usepackage{ngerman}
\usepackage[utf8]{inputenc}
\usepackage{ifthen}
\usepackage{url}
\usepackage{hyperref}

% ----------------------------------------------------------------------------
% Seitenraender, Abstaende, Zaehler
% ----------------------------------------------------------------------------
\setlength{\topmargin}{0.0cm}
\setlength{\textheight}{23.0cm}
\setlength{\textwidth}{15.0cm}
\setlength{\headsep}{0.8cm}
\setlength{\oddsidemargin}{0.9cm}
\addtolength{\evensidemargin}{-2.49cm}
\setlength{\parindent}{0pt}
\setlength{\parskip}{5pt plus 2pt minus 1pt}
\setcounter{secnumdepth}{3}
\setcounter{tocdepth}{3}
% Damit mehr Tabellen/Abbildungen auf eine Seite passen. (Kopka-Buch S. 170)
\setcounter{topnumber}{9}
\setcounter{totalnumber}{9}
% grosse Objekte nicht erst auf letzter Seite... (siehe Kopka-Buch S. 170)
\renewcommand{\topfraction}{0.99}
\renewcommand{\textfraction}{0.01}
% typische Abkuerzungen
\newcommand{\zB}{z.\,B.\ }
\newcommand{\idR}{i.\,d.\,R.\ }
\newcommand{\bzw}{bzw.\ }
\newcommand{\etc}{etc.\ }
\newcommand{\iA}{i.\,A.\ }
\newcommand{\uU}{u.\,U.\ }
\newcommand{\ua}{u.\,a.\ }
\newcommand{\usw}{usw.\ }
\renewcommand{\dh}{d.\,h.\ }
%
\clubpenalty = 10000
\widowpenalty = 10000
\displaywidowpenalty = 10000

% ----------------------------------------------------------------------------
% Schnittstellen-Parameter
% ----------------------------------------------------------------------------
% interne Makros initialisieren/vorbelegen
\newcommand{\@thesisTitle}{}
\newcommand{\@thesisType}{}
\newcommand{\@thesisAuthor}{}
\newcommand{\@thesisMail}{}
\newcommand{\@thesisGrade}{}
\newcommand{\@thesisTutor}{}
\newcommand{\@thesisDate}{}
\newcommand{\@thesisCompany}{}
% aktuelle Daten uebernehmen
\newcommand{\thesisTitle}[1]{\renewcommand{\@thesisTitle}{#1}}
\newcommand{\thesisType}[1]{\renewcommand{\@thesisType}{#1}}
\newcommand{\thesisAuthor}[1]{\renewcommand{\@thesisAuthor}{#1}}
\newcommand{\thesisMail}[1]{\renewcommand{\@thesisMail}{#1}}
\newcommand{\thesisGrade}[1]{\renewcommand{\@thesisGrade}{#1}}
\newcommand{\thesisTutor}[1]{\renewcommand{\@thesisTutor}{#1}}
\newcommand{\thesisDate}[1]{\renewcommand{\@thesisDate}{#1}}
\newcommand{\thesisCompany}[1]{\renewcommand{\@thesisCompany}{#1}}

% ----------------------------------------------------------------------------
% Initialisierung des hyperref-Pakets
% ----------------------------------------------------------------------------
\hypersetup{
  plainpages = {false}, % Seitenanker-Formatierung
  colorlinks = {true}, % Links farbig machen, sonst umrandet
  linkcolor = {blue}, % normale Links (Inhalt, Abb.)
  citecolor = {blue}, % Literaturverweise
  urlcolor = {blue}, % URL-Links
  bookmarksnumbered = {true}, % Nummerierung der Kapitel
  bookmarksopen = {false}, % alle aufgeblättert
  breaklinks = {true}
}

% ----------------------------------------------------------------------------
% Erzeugung des Deckblatts
% ----------------------------------------------------------------------------
\newcommand{\thesisMakeTitle}[0]{
  \cleardoublepage
  \pagenumbering{alph}
  \pagestyle{empty}
  \null
  \begin{center}
       \includegraphics[width=20mm]{hanfried/hanfried-de-black.pdf}
  \end{center}
  \begin{center}
    \Large
      {\huge\textbf{\@thesisTitle}\par}
    \vfill
      \textbf{-- \@thesisType{} --}
    \vfill
      \ifthenelse{\equal{\@thesisGrade}{}}{}{zur Erlangung des akademischen Grades}
    \vskip 1ex
      \@thesisGrade
    \vfill
      {\large eingereicht von} \\[1ex]
      \@thesisAuthor \\[-0.5ex]
      {\small \texttt{\@thesisMail}}
    \vfill
    {\large
      Betreuer \\[1ex]        
      \ifthenelse{\equal{\@thesisTutor}{}}{}{\@thesisTutor \\}
      Prof. Dr. habil. Wolfram Amme \\[2ex]        
      Friedrich-Schiller-Universität Jena \\
      Fakultät für Mathematik und Informatik \\
      Institut für Informatik \\
      Lehrstuhl für Softwaretechnik \\
      Ernst-Abbe-Platz 2 \\
      07743 Jena \\[2ex]
      \@thesisCompany
      \par
    }
    \vfill
      {\large \@thesisDate}
  \end{center}
  % Setup des hyperref-Pakets vervollst"andigen (Autor, Titel)
  \hypersetup{
    pdfauthor = {\@thesisAuthor}, % Author
    pdftitle = {\@thesisTitle}, % Titel
  }
}

% ----------------------------------------------------------------------------
% Erzeugung der Selbststaendigkeits-Erklaerung
% ----------------------------------------------------------------------------
\newcommand{\makeThesisDeclaration}{
  \cleardoublepage
  \thispagestyle{empty}
  ~\vfill
  \subsection*{Erklärung}
Ich versichere, dass ich die vorliegende Arbeit ohne fremde Hilfe und ohne Benutzung anderer als der angegebenen Quellen angefertigt habe. Alle Ausführungen, 
die wörtlich oder sinngemäß übernommen wurden, sind als solche gekennzeichnet. Die vorliegende Arbeit wurde in gleicher oder ähnlicher Form noch keiner anderen
Prüfungsbehörde vorgelegt und von dieser als Teil einer Prüfungsleistung angenommen. Die Richtlinien des Lehrstuhls für Examensarbeiten habe ich gelesen und anerkannt, insbesondere die Regelung des Nutzungsrechts. \\[2ex]
Jena, den \@thesisDate
} 